---
title: "GitLab Project Management - Hands-On Lab 1"
description: "This Hands-On Guide walks you through the lab exercises used in the GitLab Project Management course."
---

## Lab 1: Access The Gitlab Training Environment

> Estimated time to complete: 5 to 10 minutes

> You are viewing the latest Version 16.x instructions. If your group URL starts with https://spt.gitlabtraining.cloud, please use the [Version 15.x instructions](https://gitlab.com/gitlab-com/content-sites/handbook/-/blob/d14ee71aeac2054c72ce96e8b35ba2511f86a7ca/content/handbook/customer-success/professional-services-engineering/education-services/gitlabpmhandsonlab1.md).

## Objectives

In this lab, you will access the training lab environment. This environment will allow you to try all of the GitLab Ultimate features, providing you with an environment to follow along with all of the lab steps.

## Task A. Access your Training Group and Lab Environment

1. Navigate to [**https://gitlabdemo.com/invite**](https://gitlabdemo.com/invite) in a web browser.

1. In the **Invitation Code** field, enter the invitation code provided by your instructor or in the LevelUp LMS.

1. Select **Provision Training Environment**.

1. The system then prompts you for your **GitLab.com** username. Enter your GitLab.com user in the field provided. Select **Provision Training Environment**.

1. On the confirmation page, locate the `Your GitLab Credentials` section. Read this section carefully, noting the credential information provided and the expiration date. Your access to this group and all of the projects that you create is ephemeral and will be deleted after the expiration date.

1. Select **My Group** at the bottom of the page.

1. Sign in with your GitLab.com credentials.

1. You will be redirected to a **My Test Group** group that provides a sandbox for you to perform training lab steps in.

> This group has a GitLab Ultimate license to see all of the features while your personal username namespace requires a paid subscription or a free trial to access all of the features.

## Suggestions?

If you'd like to suggest changes to this lab, please submit them via merge request.
